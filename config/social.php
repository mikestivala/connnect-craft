<?php

return [
    'enableCpLogin' => true,

    /**
     * Allow Email Match
     */
    'allowEmailMatch' => true,

    /**
     * Login providers
     */
    'loginProviders' => [
        'google' => [
            'clientId' => '678084566246-dp082asqksjqp1nfb769lsio0jmeuq1v.apps.googleusercontent.com',
            'clientSecret' => 'zlKRROavPFLXCqvzw1EejRYs',
        ],
    ],

    /**
     * Lock social registration to specific domains
     */
    'lockDomains' => [
        'blondeandgiant.com',
    ],
];
