# SlackPeople plugin for Craft CMS 3.x

Retrieve people from slack

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require michaelstivala/slack-people

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for SlackPeople.

## SlackPeople Overview

-Insert text here-

## Configuring SlackPeople

-Insert text here-

## Using SlackPeople

-Insert text here-

## SlackPeople Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Michael Stivala](https://michaelstivala.com)
