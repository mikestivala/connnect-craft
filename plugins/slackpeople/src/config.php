<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

/**
 * SlackPeople config.php
 *
 * This file exists only as a template for the SlackPeople settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'slack-people.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "someAttribute" => true,

];
