<?php
/**
 * Slack People plugin for Craft CMS 3.x
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\elements;

use Craft;
use craft\base\Element;
use craft\elements\db\ElementQuery;
use connnect\slackpeople\SlackPeople;
use craft\elements\db\ElementQueryInterface;
use connnect\slackpeople\elements\db\EmployeeQuery;

/**
 * Employee Element
 *
 * Element is the base class for classes representing elements in terms of objects.
 *
 * @property FieldLayout|null      $fieldLayout           The field layout used by this element
 * @property array                 $htmlAttributes        Any attributes that should be included in the element’s DOM representation in the Control Panel
 * @property int[]                 $supportedSiteIds      The site IDs this element is available in
 * @property string|null           $uriFormat             The URI format used to generate this element’s URL
 * @property string|null           $url                   The element’s full URL
 * @property \Twig_Markup|null     $link                  An anchor pre-filled with this element’s URL and title
 * @property string|null           $ref                   The reference string to this element
 * @property string                $indexHtml             The element index HTML
 * @property bool                  $isEditable            Whether the current user can edit the element
 * @property string|null           $cpEditUrl             The element’s CP edit URL
 * @property string|null           $thumbUrl              The URL to the element’s thumbnail, if there is one
 * @property string|null           $iconUrl               The URL to the element’s icon image, if there is one
 * @property string|null           $status                The element’s status
 * @property Element               $next                  The next element relative to this one, from a given set of criteria
 * @property Element               $prev                  The previous element relative to this one, from a given set of criteria
 * @property Element               $parent                The element’s parent
 * @property mixed                 $route                 The route that should be used when the element’s URI is requested
 * @property int|null              $structureId           The ID of the structure that the element is associated with, if any
 * @property ElementQueryInterface $ancestors             The element’s ancestors
 * @property ElementQueryInterface $descendants           The element’s descendants
 * @property ElementQueryInterface $children              The element’s children
 * @property ElementQueryInterface $siblings              All of the element’s siblings
 * @property Element               $prevSibling           The element’s previous sibling
 * @property Element               $nextSibling           The element’s next sibling
 * @property bool                  $hasDescendants        Whether the element has descendants
 * @property int                   $totalDescendants      The total number of descendants that the element has
 * @property string                $title                 The element’s title
 * @property string|null           $serializedFieldValues Array of the element’s serialized custom field values, indexed by their handles
 * @property array                 $fieldParamNamespace   The namespace used by custom field params on the request
 * @property string                $contentTable          The name of the table this element’s content is stored in
 * @property string                $fieldColumnPrefix     The field column prefix this element’s content uses
 * @property string                $fieldContext          The field context this element’s content uses
 *
 * http://pixelandtonic.com/blog/craft-element-types
 *
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class Employee extends Element
{
    /**
     * @var string
     */
    public $designation = '';

    /**
     * @var string
     */
    public $location = '';

    /**
     * @var string
     */
    public $phone = '';

    /**
     * @var string
     */
    public $avatar = '';

    /**
     * Returns the display name of this class.
     *
     * @return string The display name of this class.
     */
    public static function displayName(): string
    {
        return Craft::t('slack-people', 'Employee');
    }

    /**
     * @inheritdoc
     */
    public static function refHandle()
    {
        return 'employee';
    }

    /**
     * Returns whether elements of this type will be storing any data in the `content`
     * table (tiles or custom fields).
     *
     * @return bool Whether elements of this type will be storing any data in the `content` table.
     */
    public static function hasContent(): bool
    {
        return true;
    }

    /**
     * Returns whether elements of this type have traditional titles.
     *
     * @return bool Whether elements of this type have traditional titles.
     */
    public static function hasTitles(): bool
    {
        return true;
    }

    /**
     * Returns whether elements of this type have statuses.
     *
     * If this returns `true`, the element index template will show a Status menu
     * by default, and your elements will get status indicator icons next to them.
     *
     * Use [[statuses()]] to customize which statuses the elements might have.
     *
     * @return bool Whether elements of this type have statuses.
     * @see statuses()
     */
    public static function isLocalized(): bool
    {
        return true;
    }

    /**
     * Creates an [[ElementQueryInterface]] instance for query purpose.
     *
     * @return ElementQueryInterface The newly created [[ElementQueryInterface]] instance.
     */
    public static function find(): ElementQueryInterface
    {
        return new EmployeeQuery(get_called_class());
    }

    /**
     * Returns whether the current user can edit the element.
     *
     * @return bool
     */
    public function getIsEditable(): bool
    {
        return true;
    }

    // Indexes, etc.
    // -------------------------------------------------------------------------

    /**
     * Returns the HTML for the element’s editor HUD.
     *
     * @return string The HTML for the editor HUD
     */
    // public function getEditorHtml(): string
    // {
    //     $html = Craft::$app->getView()->renderTemplateMacro('_includes/forms', 'textField', [
    //         [
    //             'label' => Craft::t('app', 'Title'),
    //             'siteId' => $this->siteId,
    //             'id' => 'title',
    //             'name' => 'title',
    //             'value' => $this->title,
    //             'errors' => $this->getErrors('title'),
    //             'first' => true,
    //             'autofocus' => true,
    //             'required' => true
    //         ]
    //     ]);

    //     $html .= parent::getEditorHtml();

    //     return $html;
    // }
    //
    /**
     * @inheritDoc
     */
    protected static function defineTableAttributes(): array
    {
        $attributes = [
            'title' => ['label' => Craft::t('app', 'Title')],
            'designation' => ['label' => Craft::t('app', 'Designation')],
            'location' => ['label' => Craft::t('app', 'Location')],
        ];

        return $attributes;
    }

/**
     * @inheritdoc
     */
    protected static function defineDefaultTableAttributes(string $source): array
    {
        return [
            'title',
            'designation',
            'location',
        ];
    }

    /**
     * @inheritDoc
     */
    protected static function defineSources(string $context = null): array
    {
        $sources = [];

        $sources[] = [
            'key' => '*',
            'label' => 'All Employees',
            'criteria' => []
        ];

        // Create 'Location' filters based on unique
        // locations found in the employees table.
        SlackPeople::getInstance()->people->getLocations()->each(function ($location) use (&$sources) {
            $sources[] = [
                'key' => $location,
                'label' => $location,
                'criteria' => [
                    'location' => $location
                ]
            ];
        });
        

        return $sources;
    }

    public function afterSave(bool $isNew)
    {
        if ($isNew) {
            Craft::$app->db->createCommand()
                ->insert('{{%employees}}', [
                    'id' => $this->id,
                    'designation' => $this->designation,
                    'location' => $this->location,
                    'phone' => $this->phone,
                    'avatar' => $this->avatar,
                ])
                ->execute();
        } else {
            Craft::$app->db->createCommand()
                ->update('{{%employees}}', [
                    'designation' => $this->designation,
                    'location' => $this->location,
                    'phone' => $this->phone,
                    'avatar' => $this->avatar,
                ], ['id' => $this->id])
                ->execute();
        }

        parent::afterSave($isNew);
    }
}
