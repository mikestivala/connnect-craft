<?php
namespace connnect\slackpeople\elements\db;

use craft\db\Query;
use craft\elements\db\ElementQuery;
use craft\helpers\Db;
use connnect\slackpeople\elements\Employee;

class EmployeeQuery extends ElementQuery
{
    /**
     * @var string
     */
    public $designation;

    /**
     * @var string
     */
    public $location;
    
    /**
     * @var string
     */
    public $phone;
    
    /**
     * @var string
     */
    public $avatar;

    public function designation($value)
    {
        $this->designation = $value;

        return $this;
    }

    public function location($value)
    {
        $this->location = $value;

        return $this;
    }

    public function phone($value)
    {
        $this->phone = $value;

        return $this;
    }

    public function avatar($value)
    {
        $this->avatar = $value;

        return $this;
    }

    protected function beforePrepare(): bool
    {
        // join in the products table
        $this->joinElementTable('employees');

        $this->query->select([
            'employees.designation',
            'employees.location',
            'employees.phone',
            'employees.avatar',
        ]);

        if ($this->designation) {
            $this->subQuery->andWhere(Db::parseParam('employees.designation', $this->designation));
        }

        if ($this->location) {
            $this->subQuery->andWhere(Db::parseParam('employees.location', $this->location));
        }

        if ($this->phone) {
            $this->subQuery->andWhere(Db::parseParam('employees.phone', $this->phone));
        }

        if ($this->avatar) {
            $this->subQuery->andWhere(Db::parseParam('employees.avatar', $this->avatar));
        }

        return parent::beforePrepare();
    }
}
