<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\services;

use GuzzleHttp\Client;
use craft\base\Component;
use connnect\slackpeople\SlackPeople;

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class Slack extends Component
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://slack.com/api/',
            'headers' => [
                'Authorization' => 'Bearer ' . SlackPeople::$plugin->getSettings()->apiKey
            ]
        ]);
    }

    /*
     * @return mixed
     */
    public function getUsers()
    {
        // Consider:
        // Get all the users and save them to the database.
        // The users.list method call does not list custom fields (https://api.slack.com/methods/users.list)
        // Iterate through all users in the DB and populate custom fields via the users.profile.get API call
        // Note that profile customization (like custom fields) are only avaiable to paid plans on Slack.

        return json_decode($this->client->get('users.list')->getBody())->members;
    }

    public function getUser($id)
    {
        return json_decode($this->client->post('users.profile.get', [
            'form_params' => [
                'user' => $id,
            ]
        ])->getBody());
    }
}
