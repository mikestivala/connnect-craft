<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\services;

use Craft;
use craft\elements\User;
use craft\base\Component;
use craft\elements\Entry;
use dukt\social\Plugin as Social;
use connnect\slackpeople\SlackPeople;

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class Users extends Component
{
    /**
     * Settings of dukt/social plugin
     *
     * @var array
     */
    protected $settings;

    public function __construct()
    {
        // Use the same settings as the social plugin
        $socialPlugin = Craft::$app->getPlugins()->getPlugin('social');
        $this->settings = $socialPlugin->getSettings();
    }

    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function createOrUpdateUser($user)
    {
        $craftUser = $this->getUser($user->profile->email);

        // Fill basic details
        $craftUser->firstName = property_exists($user->profile, 'first_name') ? $user->profile->first_name : '';
        $craftUser->lastName = property_exists($user->profile, 'last_name') ? $user->profile->last_name : '';
        $craftUser->username = $user->name;
        $craftUser->email = $user->profile->email;
        $craftUser->designation = property_exists($user->profile, 'title') ? $user->profile->title : '';

        // Fill employee location
        if (property_exists($user->profile, 'location') && $location = Entry::find()->section('locations')->title($user->profile->location)->one()) {
            $craftUser->employeeLocation = $location->id;
        }

        // Save user
        if (! Craft::$app->elements->saveElement($craftUser)) {
            Craft::error('There was a problem creating the user:'.print_r($craftUser->getErrors(), true), __METHOD__);
            throw new \Exception("SlackPeople couldn't save user.");
        }

        // Assign user to default group
        if (!empty($this->settings['defaultGroup'])) {
            Craft::$app->users->assignUserToGroups($craftUser->id, [$this->settings['defaultGroup']]);
            Craft::$app->elements->saveElement($craftUser);
        }

        // Save photo
        if (! $craftUser->photoId) {
            Social::$plugin->getLoginAccounts()->saveRemotePhoto($user->profile->image_512, $craftUser);
        }

        return $craftUser;
    }

    private function getUser(string $email)
    {
        $user = Craft::$app->users->getUserByUsernameOrEmail($email);

        return $user ?: new User();
    }
}
