<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\services;

use Craft;
use GuzzleHttp\Client;
use craft\elements\User;
use craft\base\Component;
use craft\elements\Entry;
use dukt\social\Plugin as Social;
use connnect\slackpeople\SlackPeople;

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class People extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function getUsers()
    {
        $usersService = SlackPeople::getInstance()->users;

        dd(collect(SlackPeople::getInstance()->slack->getUsers()));
        $users = collect(SlackPeople::getInstance()->slack->getUsers())->filter(function ($user) {
            return property_exists($user->profile, 'email');
        })->map(function ($user) use ($usersService) {
            return $usersService->createOrUpdateUser($user);
        });
        
        dd($users);
    }

    public function getLocations()
    {
        $locations = Craft::$app->db->createCommand('SELECT location from employees GROUP BY location ORDER BY location')->queryAll();

        return collect($locations)->map(function ($result) {
            return $result['location'];
        });
    }
}
