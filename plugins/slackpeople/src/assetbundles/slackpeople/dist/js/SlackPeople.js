/**
 * SlackPeople plugin for Craft CMS
 *
 * SlackPeople JS
 *
 * @author    Michael Stivala
 * @copyright Copyright (c) 2018 Michael Stivala
 * @link      https://michaelstivala.com
 * @package   SlackPeople
 * @since     1.0.0
 */
