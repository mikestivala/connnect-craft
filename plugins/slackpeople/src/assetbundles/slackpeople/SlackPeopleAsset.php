<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\assetbundles\SlackPeople;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class SlackPeopleAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@connnect/slackpeople/assetbundles/slackpeople/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/SlackPeople.js',
        ];

        $this->css = [
            'css/SlackPeople.css',
        ];

        parent::init();
    }
}
