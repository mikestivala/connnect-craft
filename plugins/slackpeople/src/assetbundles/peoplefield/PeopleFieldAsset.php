<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\assetbundles\peoplefield;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class PeopleFieldAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@connnect/slackpeople/assetbundles/peoplefield/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/People.js',
        ];

        $this->css = [
            'css/People.css',
        ];

        parent::init();
    }
}
