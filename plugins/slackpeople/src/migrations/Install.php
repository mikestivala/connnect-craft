<?php

namespace connnect\slackpeople\migrations;

use craft\db\Migration;

class Install extends Migration
{
    public function safeUp()
    {
        // if (!$this->db->tableExists('{{%employees}}')) {
        //     // create the employees table
        //     $this->createTable('{{%employees}}', [
        //         'id' => $this->integer()->notNull(),
        //         'designation' => $this->string(),
        //         'location' => $this->string(),
        //         'phone' => $this->string(),
        //         'avatar' => $this->string(),
        //         'dateCreated' => $this->dateTime()->notNull(),
        //         'dateUpdated' => $this->dateTime()->notNull(),
        //         'uid' => $this->uid(),
        //         'PRIMARY KEY(id)',
        //     ]);

        //     // give it a FK to the elements table
        //     $this->addForeignKey(
        //         $this->db->getForeignKeyName('{{%employees}}', 'id'),
        //         '{{%employees}}',
        //         'id',
        //         '{{%elements}}',
        //         'id',
        //         'CASCADE',
        //         null
        //     );
        // }
    }

    public function safeDown()
    {
        $this->dropTableIfExists('{{%employees}}');
    }
}
