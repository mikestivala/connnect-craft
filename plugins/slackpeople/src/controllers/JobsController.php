<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\controllers;

use Craft;
use craft\web\Controller;
use connnect\slackpeople\SlackPeople;
use connnect\slackpeople\jobs\SyncUsers;

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class JobsController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['sync-users'];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionSyncUsers()
    {
        Craft::$app->queue->push(new SyncUsers());
        
        if (Craft::$app->getRequest()->getIsPost()) {
            return $this->redirectToPostedUrl();
        }

        return "Done.";
    }
}
