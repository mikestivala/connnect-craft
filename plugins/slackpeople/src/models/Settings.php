<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\models;

use connnect\slackpeople\SlackPeople;

use Craft;
use craft\base\Model;

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $apiKey = '';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['apiKey', 'string'],
        ];
    }
}
