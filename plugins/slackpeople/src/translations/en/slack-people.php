<?php
/**
 * SlackPeople plugin for Craft CMS 3.x
 *
 * Retrieve people from slack
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
return [
    'SlackPeople plugin loaded' => 'SlackPeople plugin loaded',
];
