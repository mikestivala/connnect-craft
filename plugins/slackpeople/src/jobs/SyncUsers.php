<?php
/**
 * ILabCentral plugin for Craft CMS 3.x
 *
 * Integration between Beta shop and ILabCentral
 *
 * @link      https://michaelstivala.com
 * @copyright Copyright (c) 2018 Michael Stivala
 */

namespace connnect\slackpeople\jobs;

use Craft;
use craft\queue\BaseJob;
use connnect\slackpeople\SlackPeople;

/**
 * @author    Michael Stivala
 * @package   SlackPeople
 * @since     1.0.0
 */
class SyncUsers extends BaseJob
{
    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $step = 0;
        $users = $this->getUsers();
        $totalSteps = count($users);

        $usersService = SlackPeople::getInstance()->users;

        $users->each(function ($user) use ($usersService, &$step, $totalSteps, $queue) {
            $this->setProgress($queue, $step / $totalSteps);
            $step++;
            $usersService->createOrUpdateUser($user);
        });
    }

    private function getUsers()
    {
        return collect(SlackPeople::getInstance()->slack->getUsers())->filter(function ($user) {
            return property_exists($user->profile, 'email');
        });
    }

    // Protected Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    protected function defaultDescription(): string
    {
        return Craft::t('slack-people', 'Sync Users');
    }
}
